FROM golang:1.12-alpine
ADD . /app
WORKDIR /app
RUN go build -o /out
EXPOSE 8080
CMD ["/out"]